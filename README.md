# PassiveVillagerHeal

Gives all villagers regeneration 1 for 10 seconds every morning, no matter if they've slept or not.

#### Planned Features:
- Only give villagers the regeneration effect if they've slept.