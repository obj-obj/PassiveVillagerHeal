package io.obj-obj.gitlab.PassiveVillagerHeal;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class PassiveVillagerHeal extends JavaPlugin {
    private void sendMessage(String s) {
        getServer().getConsoleSender().sendMessage("[PassiveVillagerHeal] " + s);
    }

    @Override
    public void onEnable() {
        sendMessage("Enabled");
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                long time = Bukkit.getWorld("world").getTime();
                if (time > 23800 || time < 200) {
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "effect give @e[type=villager] regeneration 10 1");
                }
            }
        }, 0L, 200L);
    }

    @Override
    public void onDisable() {
        sendMessage("Disabled");
    }
}
